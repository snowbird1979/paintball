// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_PaintBall_WaldronHUD_generated_h
#error "PaintBall_WaldronHUD.generated.h already included, missing '#pragma once' in PaintBall_WaldronHUD.h"
#endif
#define PAINTBALL_WALDRON_PaintBall_WaldronHUD_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronHUD(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronHUD(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronHUD(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronHUD(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBall_WaldronHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBall_WaldronHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronHUD(APaintBall_WaldronHUD&&); \
	NO_API APaintBall_WaldronHUD(const APaintBall_WaldronHUD&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronHUD(APaintBall_WaldronHUD&&); \
	NO_API APaintBall_WaldronHUD(const APaintBall_WaldronHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBall_WaldronHUD)


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_9_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
