// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "FogPActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFogPActor() {}
// Cross Module References
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_AFogPActor_NoRegister();
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_AFogPActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintBall_Waldron();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
// End Cross Module References
	void AFogPActor::StaticRegisterNativesAFogPActor()
	{
	}
	UClass* Z_Construct_UClass_AFogPActor_NoRegister()
	{
		return AFogPActor::StaticClass();
	}
	UClass* Z_Construct_UClass_AFogPActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_PaintBall_Waldron,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "FogPActor.h" },
				{ "ModuleRelativePath", "FogPActor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fog_p_MetaData[] = {
				{ "Category", "FogPActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "FogPActor.h" },
				{ "ToolTip", "Used for initializing new fog particle actor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_fog_p = { UE4CodeGen_Private::EPropertyClass::Object, "fog_p", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080009, 1, nullptr, STRUCT_OFFSET(AFogPActor, fog_p), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(NewProp_fog_p_MetaData, ARRAY_COUNT(NewProp_fog_p_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fog_p,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AFogPActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AFogPActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFogPActor, 2811260180);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFogPActor(Z_Construct_UClass_AFogPActor, &AFogPActor::StaticClass, TEXT("/Script/PaintBall_Waldron"), TEXT("AFogPActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFogPActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
