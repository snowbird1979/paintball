// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_StoneCubeActor_generated_h
#error "StoneCubeActor.generated.h already included, missing '#pragma once' in StoneCubeActor.h"
#endif
#define PAINTBALL_WALDRON_StoneCubeActor_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStoneCubeActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AStoneCubeActor(); \
public: \
	DECLARE_CLASS(AStoneCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AStoneCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAStoneCubeActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AStoneCubeActor(); \
public: \
	DECLARE_CLASS(AStoneCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AStoneCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStoneCubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStoneCubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneCubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneCubeActor(AStoneCubeActor&&); \
	NO_API AStoneCubeActor(const AStoneCubeActor&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneCubeActor(AStoneCubeActor&&); \
	NO_API AStoneCubeActor(const AStoneCubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneCubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStoneCubeActor)


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AStoneCubeActor, mesh); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AStoneCubeActor, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AStoneCubeActor, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AStoneCubeActor, m_dynamicMaterialInstance); }


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_12_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_StoneCubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
