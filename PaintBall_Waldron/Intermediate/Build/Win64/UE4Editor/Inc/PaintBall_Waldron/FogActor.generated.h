// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_FogActor_generated_h
#error "FogActor.generated.h already included, missing '#pragma once' in FogActor.h"
#endif
#define PAINTBALL_WALDRON_FogActor_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFogActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AFogActor(); \
public: \
	DECLARE_CLASS(AFogActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AFogActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFogActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AFogActor(); \
public: \
	DECLARE_CLASS(AFogActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AFogActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFogActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFogActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFogActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFogActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFogActor(AFogActor&&); \
	NO_API AFogActor(const AFogActor&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFogActor(AFogActor&&); \
	NO_API AFogActor(const AFogActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFogActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFogActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFogActor)


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFogActor, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFogActor, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFogActor, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFogActor, m_dynamicMaterialInstance); }


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_10_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_FogActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
