// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_CubeActor_Procedural_generated_h
#error "CubeActor_Procedural.generated.h already included, missing '#pragma once' in CubeActor_Procedural.h"
#endif
#define PAINTBALL_WALDRON_CubeActor_Procedural_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACubeActor_Procedural(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_ACubeActor_Procedural(); \
public: \
	DECLARE_CLASS(ACubeActor_Procedural, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor_Procedural) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_INCLASS \
private: \
	static void StaticRegisterNativesACubeActor_Procedural(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_ACubeActor_Procedural(); \
public: \
	DECLARE_CLASS(ACubeActor_Procedural, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor_Procedural) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACubeActor_Procedural(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACubeActor_Procedural) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor_Procedural); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor_Procedural); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor_Procedural(ACubeActor_Procedural&&); \
	NO_API ACubeActor_Procedural(const ACubeActor_Procedural&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor_Procedural(ACubeActor_Procedural&&); \
	NO_API ACubeActor_Procedural(const ACubeActor_Procedural&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor_Procedural); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor_Procedural); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACubeActor_Procedural)


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ACubeActor_Procedural, mesh); }


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_10_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_CubeActor_Procedural_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
