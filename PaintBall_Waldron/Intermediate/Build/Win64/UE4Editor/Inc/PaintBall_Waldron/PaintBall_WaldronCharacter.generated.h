// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_PaintBall_WaldronCharacter_generated_h
#error "PaintBall_WaldronCharacter.generated.h already included, missing '#pragma once' in PaintBall_WaldronCharacter.h"
#endif
#define PAINTBALL_WALDRON_PaintBall_WaldronCharacter_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronCharacter(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronCharacter(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronCharacter(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronCharacter(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBall_WaldronCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBall_WaldronCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronCharacter(APaintBall_WaldronCharacter&&); \
	NO_API APaintBall_WaldronCharacter(const APaintBall_WaldronCharacter&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronCharacter(APaintBall_WaldronCharacter&&); \
	NO_API APaintBall_WaldronCharacter(const APaintBall_WaldronCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBall_WaldronCharacter)


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintBall_WaldronCharacter, L_MotionController); }


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_12_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
