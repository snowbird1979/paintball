// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_WALDRON_FogPActor_generated_h
#error "FogPActor.generated.h already included, missing '#pragma once' in FogPActor.h"
#endif
#define PAINTBALL_WALDRON_FogPActor_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_RPC_WRAPPERS
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFogPActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AFogPActor(); \
public: \
	DECLARE_CLASS(AFogPActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AFogPActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFogPActor(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_AFogPActor(); \
public: \
	DECLARE_CLASS(AFogPActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(AFogPActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFogPActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFogPActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFogPActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFogPActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFogPActor(AFogPActor&&); \
	NO_API AFogPActor(const AFogPActor&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFogPActor(AFogPActor&&); \
	NO_API AFogPActor(const AFogPActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFogPActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFogPActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFogPActor)


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_PRIVATE_PROPERTY_OFFSET
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_10_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_FogPActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
