// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBALL_WALDRON_PaintBall_WaldronProjectile_generated_h
#error "PaintBall_WaldronProjectile.generated.h already included, missing '#pragma once' in PaintBall_WaldronProjectile.h"
#endif
#define PAINTBALL_WALDRON_PaintBall_WaldronProjectile_generated_h

#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronProjectile(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronProjectile(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBall_WaldronProjectile(); \
	friend PAINTBALL_WALDRON_API class UClass* Z_Construct_UClass_APaintBall_WaldronProjectile(); \
public: \
	DECLARE_CLASS(APaintBall_WaldronProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PaintBall_Waldron"), NO_API) \
	DECLARE_SERIALIZER(APaintBall_WaldronProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBall_WaldronProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBall_WaldronProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronProjectile(APaintBall_WaldronProjectile&&); \
	NO_API APaintBall_WaldronProjectile(const APaintBall_WaldronProjectile&); \
public:


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBall_WaldronProjectile(APaintBall_WaldronProjectile&&); \
	NO_API APaintBall_WaldronProjectile(const APaintBall_WaldronProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBall_WaldronProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBall_WaldronProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBall_WaldronProjectile)


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintBall_WaldronProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintBall_WaldronProjectile, ProjectileMovement); }


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_9_PROLOG
#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_RPC_WRAPPERS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_INCLASS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_INCLASS_NO_PURE_DECLS \
	PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintBall_Waldron_Source_PaintBall_Waldron_PaintBall_WaldronProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
