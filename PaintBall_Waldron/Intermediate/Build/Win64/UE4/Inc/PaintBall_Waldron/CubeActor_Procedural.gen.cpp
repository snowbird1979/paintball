// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "CubeActor_Procedural.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCubeActor_Procedural() {}
// Cross Module References
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_ACubeActor_Procedural_NoRegister();
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_ACubeActor_Procedural();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintBall_Waldron();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void ACubeActor_Procedural::StaticRegisterNativesACubeActor_Procedural()
	{
	}
	UClass* Z_Construct_UClass_ACubeActor_Procedural_NoRegister()
	{
		return ACubeActor_Procedural::StaticClass();
	}
	UClass* Z_Construct_UClass_ACubeActor_Procedural()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_PaintBall_Waldron,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "CubeActor_Procedural.h" },
				{ "ModuleRelativePath", "CubeActor_Procedural.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mesh_MetaData[] = {
				{ "BlueprintType", "true" },
				{ "Category", "CubeActor_Procedural" },
				{ "EditInline", "true" },
				{ "IsBlueprintBase", "true" },
				{ "ModuleRelativePath", "CubeActor_Procedural.h" },
				{ "ToolTip", "Declared mesh object that will be constructed and modified to create cube like shape." },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(ACubeActor_Procedural, mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_mesh_MetaData, ARRAY_COUNT(NewProp_mesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_mesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACubeActor_Procedural>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACubeActor_Procedural::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACubeActor_Procedural, 4271150954);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACubeActor_Procedural(Z_Construct_UClass_ACubeActor_Procedural, &ACubeActor_Procedural::StaticClass, TEXT("/Script/PaintBall_Waldron"), TEXT("ACubeActor_Procedural"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACubeActor_Procedural);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
