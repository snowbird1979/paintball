// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PaintBall_WaldronGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintBall_WaldronGameMode() {}
// Cross Module References
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_APaintBall_WaldronGameMode_NoRegister();
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_APaintBall_WaldronGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PaintBall_Waldron();
// End Cross Module References
	void APaintBall_WaldronGameMode::StaticRegisterNativesAPaintBall_WaldronGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaintBall_WaldronGameMode_NoRegister()
	{
		return APaintBall_WaldronGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintBall_WaldronGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_PaintBall_Waldron,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "PaintBall_WaldronGameMode.h" },
				{ "ModuleRelativePath", "PaintBall_WaldronGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintBall_WaldronGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintBall_WaldronGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintBall_WaldronGameMode, 2542409180);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintBall_WaldronGameMode(Z_Construct_UClass_APaintBall_WaldronGameMode, &APaintBall_WaldronGameMode::StaticClass, TEXT("/Script/PaintBall_Waldron"), TEXT("APaintBall_WaldronGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintBall_WaldronGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
