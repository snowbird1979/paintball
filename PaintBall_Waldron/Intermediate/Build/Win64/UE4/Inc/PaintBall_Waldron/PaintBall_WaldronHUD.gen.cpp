// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PaintBall_WaldronHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintBall_WaldronHUD() {}
// Cross Module References
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_APaintBall_WaldronHUD_NoRegister();
	PAINTBALL_WALDRON_API UClass* Z_Construct_UClass_APaintBall_WaldronHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_PaintBall_Waldron();
// End Cross Module References
	void APaintBall_WaldronHUD::StaticRegisterNativesAPaintBall_WaldronHUD()
	{
	}
	UClass* Z_Construct_UClass_APaintBall_WaldronHUD_NoRegister()
	{
		return APaintBall_WaldronHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintBall_WaldronHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_PaintBall_Waldron,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "PaintBall_WaldronHUD.h" },
				{ "ModuleRelativePath", "PaintBall_WaldronHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintBall_WaldronHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintBall_WaldronHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintBall_WaldronHUD, 2070342808);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintBall_WaldronHUD(Z_Construct_UClass_APaintBall_WaldronHUD, &APaintBall_WaldronHUD::StaticClass, TEXT("/Script/PaintBall_Waldron"), TEXT("APaintBall_WaldronHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintBall_WaldronHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
