// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PaintBall_Waldron : ModuleRules
{
	public PaintBall_Waldron(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        //This code allows the program to access the default modules and the ProceduralMeshComponent and RuntimeMeshComponent modules
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "ProceduralMeshComponent", "RuntimeMeshComponent" });

        //
        PrivateDependencyModuleNames.AddRange(new string[] { "RHI", "RenderCore" });
    }
}
