// Fill out your copyright notice in the Description page of Project Settings.

#include "FogPActor.h"
#include "PaintBall_WaldronCharacter.h"


// Sets default values
AFogPActor::AFogPActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create default fog subobject and assign it to root component
	fog_p = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	RootComponent = fog_p;

}

// Called when the game starts or when spawned
void AFogPActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFogPActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Cast to first person character and get players location
	APaintBall_WaldronCharacter* playerChar = Cast<APaintBall_WaldronCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	FVector Loc = playerChar->GetActorLocation();

	//Set the fog vector parameter while using players location for Point Attractor in Particle System
	if (fog_p)
	{
		fog_p->SetVectorParameter("KickFog", Loc);
	}








}

