// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "PaintBall_WaldronGameMode.h"
#include "PaintBall_WaldronHUD.h"
#include "PaintBall_WaldronCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintBall_WaldronGameMode::APaintBall_WaldronGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintBall_WaldronHUD::StaticClass();
}
