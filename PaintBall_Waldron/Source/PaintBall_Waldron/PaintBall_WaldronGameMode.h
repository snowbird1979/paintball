// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintBall_WaldronGameMode.generated.h"

UCLASS(minimalapi)
class APaintBall_WaldronGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintBall_WaldronGameMode();
};



