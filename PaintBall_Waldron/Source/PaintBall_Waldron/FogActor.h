// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "FogActor.generated.h"

UCLASS()
class PAINTBALL_WALDRON_API AFogActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFogActor();

	virtual void PostInitializeComponents() override;

	// Set the fog size  
	void setSize(float s);

	// Reveal a portion of the fog  
	void revealSmoothCircle(const FVector2D & pos, float radius);

private:

	//initialize method for updating fog textures
	void UpdateTextureRegions(UTexture2D * Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D * Regions, uint32 SrcPitch, uint32 SrcBpp, uint8 * SrcData, bool bFreeData);

	// Fog texture size  
	static const int m_textureSize = 256;

	//This code is used for specifying texture, material instance, material, and mesh components to being used in .cpp file. Also code specifies an array for storing texure information and sizes
	UPROPERTY() 
		UStaticMeshComponent * m_squarePlane;
	UPROPERTY() 
		UTexture2D * m_dynamicTexture;
	UPROPERTY() 
		UMaterialInterface * m_dynamicMaterial;
	UPROPERTY() 
		UMaterialInstanceDynamic * m_dynamicMaterialInstance;
	uint8 m_pixelArray[m_textureSize * m_textureSize];
	FUpdateTextureRegion2D m_wholeTextureRegion;
	float m_coverSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
};
